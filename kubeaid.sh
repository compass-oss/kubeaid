#!/bin/bash

# Color configuration
INDEX_COLOR="\e[36m"    # Cyan
CURRENT_COLOR="\e[32m"  # Green
NAMESPACE_COLOR="\e[34m" # Blue
BOLD="\e[1m"
RESET_COLOR="\e[0m"

# Save the original PS1
original_ps1=$PS1

# Function to get the current kubectl context
current_kubectl_context() {
    kubectl config current-context 2>/dev/null
}

# Function to get the current namespace
current_kubectl_namespace() {
    kubectl config view -o jsonpath="{.contexts[?(@.name == \"$(kubectl config current-context)\")].context.namespace}" 2>/dev/null
}


# Function to reset the command prompt to its original state
kstop() {
    PS1=$original_ps1
}

# Global array to store namespaces
declare -a KUBE_NAMESPACES

# Function to list all namespaces with indices and highlight the current namespace
kn_list() {
    local current_namespace=$(current_kubectl_namespace)
    KUBE_NAMESPACES=($(kubectl get namespaces --no-headers -o custom-columns=":metadata.name"))
    for i in "${!KUBE_NAMESPACES[@]}"; do
        if [ "${KUBE_NAMESPACES[$i]}" = "$current_namespace" ]; then
            echo -e "\t$BOLD[$i]$RESET_COLOR: $NAMESPACE_COLOR${KUBE_NAMESPACES[$i]}$RESET_COLOR (current)"
        else
            echo -e "\t$BOLD[$i]$RESET_COLOR: ${KUBE_NAMESPACES[$i]}"
        fi
    done
}

# Function to set a given namespace by name or index
kn_set() {
    if [[ $1 =~ ^[0-9]+$ ]] && [ "$1" -ge 0 ] && [ "$1" -lt "${#KUBE_NAMESPACES[@]}" ]; then
        kubectl config set-context --current --namespace "${KUBE_NAMESPACES[$1]}"
    elif [ -n "$1" ]; then
        kubectl config set-context --current --namespace "$1"
    else
        echo "Invalid namespace: $1"
    fi
}

# Function to handle kn command
kn() {
    if [ "$#" -eq 0 ]; then
        kn_list
    else
        kn_set "$1"
    fi
}

# Function to list all contexts with indices and highlight the current context
kx_list() {
    local current_context=$(current_kubectl_context)
    KUBE_CONTEXTS=($(kubectl config get-contexts -o name))
    for i in "${!KUBE_CONTEXTS[@]}"; do
        if [ "${KUBE_CONTEXTS[$i]}" = "$current_context" ]; then
            echo -e "\t$BOLD[$i]$RESET_COLOR: $CURRENT_COLOR${KUBE_CONTEXTS[$i]}$RESET_COLOR (current)"
        else
            echo -e "\t$BOLD[$i]$RESET_COLOR: ${KUBE_CONTEXTS[$i]}"
        fi
    done
}

# Function to set a given kubectl context by name or index
kx_set() {
    if [[ $1 =~ ^[0-9]+$ ]] && [ "$1" -ge 0 ] && [ "$1" -lt "${#KUBE_CONTEXTS[@]}" ]; then
        kubectl config use-context "${KUBE_CONTEXTS[$1]}"
    elif [ -n "$1" ]; then
        kubectl config use-context "$1"
    else
        echo "Invalid context: $1"
    fi
}


# Function to handle kx command
kx() {
    if [ "$#" -eq 0 ]; then
        kx_list
    else
        kx_set "$1"
    fi
}


# Modify PS1 to include colored Kubernetes context and namespace
kstart() {
    PS1="\[\e[1;32m\]\$(current_kubectl_context)\[\e[0m\]/\[\e[34m\]\$(current_kubectl_namespace)\[\e[0m\]:\w\$ "
}

alias k='kubectl'

# Function for autocompleting kubectl logs -f
_kubectl_logs() {
    local cur prev cmd
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    cmd="${COMP_WORDS[1]}"

    # Check if the command is 'logs' and handle autocompletion
    if [[ "$cmd" == "logs" ]]; then
        if [[ "$prev" == "-f" ]] || [[ "$COMP_CWORD" -eq 2 ]]; then
            local pods=$(kubectl get pods --no-headers -o custom-columns=":metadata.name" | grep "^$cur")
            COMPREPLY=($(compgen -W "${pods}" -- "$cur"))
        fi
    fi
}

# Enable autocomplete for k logs
complete -F _kubectl_logs k