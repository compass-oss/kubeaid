# KubeAID

## Overview

KubeAID is a tool designed to enhance the Kubernetes command-line experience. The code was completely written by AI as
an experiment. It simplifies Kubernetes operations by providing handy utilities and shortcuts for managing contexts 
and namespaces, along with intelligent autocompletion features.

## Features

- `k`: Alias for `kubectl`.
- `kx`: List Kubernetes contexts or switch to a context by name or index.
- `kn`: List Kubernetes namespaces or switch to a namespace by name or index.
- `kstart`: Update the command prompt to show the current Kubernetes context and namespace.
- `kstop`: Revert the command prompt to its original state.
- Intelligent autocompletion for `k logs -f <pod-name>`.

## Customization

The script allows easy customization of colors used in the prompt and output:

- `INDEX_COLOR`: Color for indices (default: Cyan).
- `CURRENT_COLOR`: Color for highlighting the current context or namespace (default: Green).
- `NAMESPACE_COLOR`: Color for namespaces in the prompt (default: Blue).
- `BOLD`: Style for making text bold.
- `RESET_COLOR`: Resets the color/style to default.

Modify these variables at the top of the script to customize the color scheme.

## Usage

1. Source the script in your shell session.
2. Use `kx` to list or switch contexts, `kn` to list or switch namespaces, and `k` as a shorthand for `kubectl`.
3. Use `kstart` to enable dynamic prompt updates, and `kstop` to disable them.
4. Enjoy enhanced autocompletion for Kubernetes commands.

## Example

```bash
$ kx
0: my-context (current)
1: other-context
$ kx 1
Switched to context "other-context".
$ kn
0: default
1: my-namespace (current)
$ kn 0
Switched to namespace "default".
$ k logs -f [Tab for autocompletion]
```